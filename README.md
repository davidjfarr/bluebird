# README #

This is the README for the BlueBird web application.

### What is BlueBird? ###

The BlueBird project aims to provide individuals suffering from anxiety or signs of depression to reach out and chat to qualified helpers through the comfort of the internet and their keyboard. 

BlueBird has the following four components

* Anonymous users and staff authentication
* Queue Management System with matching algorithm
* Chat system with web sockets
* Post-chat support network

### How do I get set up? ###

* Requirements

    * Spring Boot (version 4.0.0)
    * Apache Maven (version 3+) 
    * Java Version 8 (minimum)

* Dependencies

All dependencies are listed in pom.xml. These need to be downloaded and generated by Maven before running the application.

* Database configuration

BlueBird requires a local instance of MySQL server to be running, with a "BlueBird" schema existing as well. 

* Deployment instructions

elec5619.bluebird.BluebirdApplication is the main application class. This needs to be compiled and run in order to start the application.

### How do I use BlueBird? ###

There are three types of users in BlueBird: Staff, Anonymous Clients and Administrators. This Readme will outline the usage scenarios applicable by each type of user.

#### Clients ####

Clients start using BlueBird by visiting the website's homepage (`/`). Here on the home page they are presented with a large start chatting anonymously button. Once they click on this they are passed on to the queue landing page.

![Alt-text](http://i66.tinypic.com/wb5hef.png "Optional title")


On the landing page (`/queue/land`), clients are asked to enter a preferred username and to describe what they want to talk about. After typing in this info, they click the *Join Queue* button to be sent into the queue.

![Alt-text](http://i67.tinypic.com/23kchsk.png "Optional title")

Once in the queue (`/queue`), clients are presented with an engaging and informative view. As shown in the below screenshot, clients see a progress indicator on the bottom of their screen which allows them to easily gauge how long until they will likely get into a chat session. Additionally, and this is the main body of the presented screen, is an informative slider that contains engaging images and facts that the user can read whilst they wait. 

![Alt-text](http://i68.tinypic.com/15nm008.png "Optional title")

Once they have been matched with a free staff member, the user will be redirected to begin a chat session (`/chat`). Here they can message simultaneously with the staff to talk about their nominated topic.

![Alt-text](http://i65.tinypic.com/xnhggi.png "Optional title")

Once a chat session is finished, the client is redirected to (`/finish`) and presented with options for seeking further help or follow up.

#### Staff ####

Staff members need to log in through the endpoint (`/staff-login`). Once they have log in, they will immediately see a list of users in the queue, ranked by the "severity" of the tags they have selected. From here, they can initiate a chat with a user by clicking "start chat". This takes them to the chat page. Once the chat is finished, they can click "disconnect" to leave. This redirects back to the "view queue" page 

![Screen Shot 2016-10-25 at 4.29.20 pm.png](https://bitbucket.org/repo/kgLg7e/images/994209803-Screen%20Shot%202016-10-25%20at%204.29.20%20pm.png)

![Screen Shot 2016-10-25 at 4.34.15 pm.png](https://bitbucket.org/repo/kgLg7e/images/2083209790-Screen%20Shot%202016-10-25%20at%204.34.15%20pm.png)

Staff can also edit their profile by clicking 'Edit Profile' in the queue view.

![Screen Shot 2016-10-25 at 4.31.06 pm.png](https://bitbucket.org/repo/kgLg7e/images/3883343474-Screen%20Shot%202016-10-25%20at%204.31.06%20pm.png)

#### Admin ####

Administrator users are users that can manage staff accounts. They can login at (`/staff-login`) and can create new users on their landing page. Once they create a new user, they are logged out and the new staff member can log in and edit their profile.

![Screen Shot 2016-10-25 at 4.40.06 pm.png](https://bitbucket.org/repo/kgLg7e/images/3433044765-Screen%20Shot%202016-10-25%20at%204.40.06%20pm.png)

### More Information ###

* Please consult the wiki for an in-depth exploration of all components and their functionality.