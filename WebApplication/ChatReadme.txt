Chat Information - to be merged with main README.TXT

STAFF: INIT A CHAT - Log in, and pop a user off the queue. This will initialise a chat.

USER: JOINING A CHAT - join the queue. Once a staff member pops you off, you will join the chat.

Access Points

1) /messaging/chatland
    This is a dummy start point for chat - a "staff" can create a new chat session, which a user can join. This is soon to be deprecated in favour of the automated process from queue.

2) /messaging/newchat
    This is the endpoint that is accessed first when a staff member "pops" a user from the queue, creating a new chat
    It also redirects the staffmember to the newly created chat room

3) /messaging/joinchat
    This is the endpoint for the user to join the chat once they have been popper off the queue
    It redirects the user to the correct room (/messaging/roomid)

4) /messaging/{roomid}
    This is the final messaging endpoint, where roomid corresponds to the ID of the room created.
    This is where the chat window appears and a websocket connection is opened between the two parties.

Important files

chat.js - main websocket logic is done through this file.
MessageController.js - Holds all the mappings and conversions for the above.

