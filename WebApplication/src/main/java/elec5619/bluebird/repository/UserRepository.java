package elec5619.bluebird.repository;

import elec5619.bluebird.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findBySessionId(String sessionId);
}
