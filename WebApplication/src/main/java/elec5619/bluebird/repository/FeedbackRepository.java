package elec5619.bluebird.repository;

import elec5619.bluebird.domain.Feedback;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David on 16/10/2016.
 */

@EntityScan(value="elec5619.bluebird.model.Feedback")

@Repository
@Transactional
public interface FeedbackRepository extends CrudRepository<Feedback, Integer> {}
