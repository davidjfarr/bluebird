package elec5619.bluebird.repository;

import elec5619.bluebird.domain.QueueStaff;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Josh on 17/10/2016.
 */
public interface QueueStaffRepository extends CrudRepository<QueueStaff, Long> {
}
