package elec5619.bluebird.repository;

import elec5619.bluebird.domain.Profile;
import elec5619.bluebird.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Profile findProfileByUser(User user);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM Profile p WHERE p.user = :user")
    boolean userHasProfile(@Param("user") User user);
}
