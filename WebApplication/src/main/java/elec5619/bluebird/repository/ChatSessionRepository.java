package elec5619.bluebird.repository;

import elec5619.bluebird.domain.ChatSession;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by yoshi on 22/10/2016.
 */
public interface ChatSessionRepository extends CrudRepository<ChatSession, Long> {
    List<ChatSession> findByUserSessionId (String userSessionId);
    List<ChatSession> findByStaffSessionId (String staffSessionId);
}