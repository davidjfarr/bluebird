package elec5619.bluebird.repository;

import elec5619.bluebird.domain.QueueClient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Josh on 14/10/2016.
 */
public interface QueueClientRepository extends CrudRepository<QueueClient, Long> {
    List<QueueClient> findBySessionId(String SessionId);

    // This will find users in the order they were added (based on time)
    @Query("SELECT qc from QueueClient qc ORDER BY startTime")
    List<QueueClient> findInOrderAdded();
}