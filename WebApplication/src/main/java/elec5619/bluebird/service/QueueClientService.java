package elec5619.bluebird.service;

import elec5619.bluebird.domain.QueueClient;

import java.util.List;

/**
 * Created by Josh on 12/10/2016.
 */
public interface QueueClientService {

    // Sets the progress of a client with id to 100% & returns which one it was to it can be passed to chat
    QueueClient setClientFinishedWithId(long id);

    // Simply returns a client with the associated id
    QueueClient findClientWithId(long id);

    // Finds all clients with a specific session id
    List<QueueClient> findClientsWithSessionId(String sessionId);

    // Saves any QueueClient objects
    void saveClient(QueueClient queueClient);

    // Retrieve any relevant queueClients and remove them
    void removeClientsWithSessionId(String sessionId);

    // Finds a list of all queue clients, ordered by longest to shortest waiting time
    List<QueueClient> findClientsInOrderAdded();

}
