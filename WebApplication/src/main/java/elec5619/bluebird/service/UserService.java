package elec5619.bluebird.service;

import elec5619.bluebird.domain.User;

public interface UserService {
    void saveStaff(User user);
    void saveAdmin(User user);
    User findByUsername(String username);
}
