package elec5619.bluebird.service;

import elec5619.bluebird.domain.ChatSession;
import elec5619.bluebird.domain.QueueStaff;
import elec5619.bluebird.domain.QueueTag;

/**
 * Created by Josh on 12/10/2016.
 */
public interface QueueStaffService {

    // Simply returns a staff member with the associated id
    QueueStaff findStaffWithId(long id);

    // Saves any QueueClient objects
    void saveStaff(QueueStaff queueStaff);

    // Remove a QueueClient object when it is no longer necessary
    void deleteStaff(QueueStaff queueStaff);

    // Retrieves a standard list of tags
    Iterable<QueueTag> findAllCommonTags();

    // Saves a chat session
    void saveChatSession(ChatSession chatSession);

}
