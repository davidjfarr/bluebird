package elec5619.bluebird.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}