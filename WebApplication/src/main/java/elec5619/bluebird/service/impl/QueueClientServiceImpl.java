package elec5619.bluebird.service.impl;

import elec5619.bluebird.domain.QueueClient;
import elec5619.bluebird.repository.QueueClientRepository;
import elec5619.bluebird.service.QueueClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Josh on 12/10/2016.
 */
@Service
public class QueueClientServiceImpl implements QueueClientService {

    private final QueueClientRepository queueClientRepository;

    @Autowired
    public QueueClientServiceImpl(final QueueClientRepository queueClientRepository) {
        this.queueClientRepository = queueClientRepository;
    }

    @Transactional
    public QueueClient setClientFinishedWithId(long id) {
        QueueClient queueClient = queueClientRepository.findOne(id);
        if (queueClient != null) {
            queueClient.setProgress(1000);
            queueClientRepository.save(queueClient);
        }
        return queueClient;
    }

    @Transactional
    public QueueClient findClientWithId(long id) {
        return queueClientRepository.findOne(id);
    }

    @Transactional
    public List<QueueClient> findClientsWithSessionId(String sessionId) {
        return queueClientRepository.findBySessionId(sessionId);
    }

    @Transactional
    public void saveClient(QueueClient queueClient) {
        queueClientRepository.save(queueClient);
    }

    @Transactional
    public void removeClientsWithSessionId(String sessionId) {
        List<QueueClient> queueClients = queueClientRepository.findBySessionId(sessionId);
        queueClientRepository.delete(queueClients);
    }

    @Transactional
    public List<QueueClient> getTopQueue() {
        return null;
    }

    @Transactional
    public List<QueueClient> findClientsInOrderAdded() {
        return queueClientRepository.findInOrderAdded();
    }
}
