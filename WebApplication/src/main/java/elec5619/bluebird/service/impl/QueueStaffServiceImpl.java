package elec5619.bluebird.service.impl;

import elec5619.bluebird.domain.ChatSession;
import elec5619.bluebird.domain.QueueStaff;
import elec5619.bluebird.domain.QueueTag;
import elec5619.bluebird.repository.ChatSessionRepository;
import elec5619.bluebird.repository.QueueStaffRepository;
import elec5619.bluebird.repository.QueueTagRepository;
import elec5619.bluebird.service.QueueStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Josh on 24/10/16.
 */

@Service
public class QueueStaffServiceImpl implements QueueStaffService {


    private final QueueStaffRepository queueStaffRepository;
    private final ChatSessionRepository chatSessionRepository;
    private final QueueTagRepository queueTagRepository;

    @Autowired
    public QueueStaffServiceImpl(QueueStaffRepository queueStaffRepository, ChatSessionRepository chatSessionRepository, QueueTagRepository queueTagRepository){
        this.queueStaffRepository = queueStaffRepository;
        this.chatSessionRepository = chatSessionRepository;
        this.queueTagRepository = queueTagRepository;
    }

    @Transactional
    public QueueStaff findStaffWithId(long id) {
        return queueStaffRepository.findOne(id);
    }

    @Transactional
    public void saveStaff(QueueStaff queueStaff) {
        queueStaffRepository.save(queueStaff);
    }

    @Transactional
    public void deleteStaff(QueueStaff queueStaff) {
        queueStaffRepository.delete(queueStaff);
    }

    @Transactional
    public Iterable<QueueTag> findAllCommonTags() {
        return queueTagRepository.findAll();
    }

    @Transactional
    public void saveChatSession(ChatSession chatSession) {
        chatSessionRepository.save(chatSession);
    }
}
