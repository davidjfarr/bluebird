package elec5619.bluebird.controller;

import elec5619.bluebird.domain.*;
import elec5619.bluebird.repository.ChatSessionRepository;
import elec5619.bluebird.repository.FeedbackRepository;
import elec5619.bluebird.repository.ProfileRepository;
import elec5619.bluebird.repository.UserRepository;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.*;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.RequestContextHolder;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Scope("session")
public class PostChatController {
    private String staffName;
    private String userName;
    private String staffEmail;

    private FeedbackRepository feedbackRepository;
    private ChatSessionRepository chatRepository;
    private UserRepository userRepository;
    private ProfileRepository profileRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    PostChatController(FeedbackRepository feedbackRepository,
                       UserRepository userRepository,
                       ChatSessionRepository chatRepository,
                       ProfileRepository profileRepository) {
        this.feedbackRepository = feedbackRepository;
        this.userRepository = userRepository;
        this.chatRepository = chatRepository;
        this.profileRepository = profileRepository;
    }

    @GetMapping("/finish")
    public String index(Model model) {
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        List<ChatSession> chatSessions = chatRepository.findByUserSessionId(sessionId);
        ChatSession session = new ChatSession();

        for (ChatSession i: chatSessions) {
            System.out.println("chat session id: " + i.getUserSessionId());
            if (i.getUserSessionId().equals(sessionId)) {
                session = i;
                break;
            }
        }

        User staff = userRepository.findByUsername(session.getStaffName());

        staffName = staff.getUsername();
        userName = session.getUserName();
        staffEmail = staff.getEmail();

        return "postchathome";
    }

    //Show user nearby mental health services
    @GetMapping("/finish/around-you")
    public String aroundYou(Model model, HttpServletRequest request) {
        //Get IP address from user
        String ipAddress = request.getRemoteAddr();
        model.addAttribute("ipAddress", ipAddress);
        return "around-you";
    }

    //Present user with feedback form
    @GetMapping("/finish/feedback")
    public String feedback(Model model) {
        model.addAttribute("feedback", new Feedback());
        return "feedback";
    }

    //Process feedback form
    @PostMapping("/finish/feedback")
    public String processFeedback(@ModelAttribute Feedback feedback) {
        //Get the current date and format it as a string
        Date current = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        feedback.setStaff(staffName);
        feedback.setUser(userName);
        feedback.setDate(format.format(current));

        //Add to database
        feedbackRepository.save(feedback);

        return "complete";
    }

    //Preset user with contact form
    @GetMapping("/finish/contact")
    public String email(Model model) {
        model.addAttribute("email", new Email());
        return "contact";
    }

    //Process contact form
    @PostMapping("/finish/contact")
    public String processEmail(@ModelAttribute Email email) {
        System.out.println("Sending message to: " + staffEmail);

        Date current = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        MimeMessage msg = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(msg);
            helper.setTo(staffEmail);
            helper.setSubject("Message from your chat with user " + userName + "on " + format.format(current));
            helper.setFrom("getincontactbluebird@gmail.com");
            helper.setText(email.getContent());

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        mailSender.send(msg);

        return "complete";
    }

    //Show user related information
    @GetMapping("/finish/info")
    public String info() { return "info"; }

}
