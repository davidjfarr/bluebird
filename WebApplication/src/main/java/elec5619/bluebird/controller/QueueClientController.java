package elec5619.bluebird.controller;

import elec5619.bluebird.domain.QueueClient;
import elec5619.bluebird.service.QueueClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.*;

/**
 * Created by Josh on 12/10/2016.
 */

@Controller
public class QueueClientController {

    QueueClientService queueClientService;

    @Autowired
    public QueueClientController(QueueClientService queueClientService) {
        this.queueClientService = queueClientService;
    }

    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    // Present the queue session for the respective client
    @RequestMapping("/queue/{id}")
    public String queue(@PathVariable Long id, Model model) {
        // Load QueueClient
        QueueClient queueClient = queueClientService.findClientWithId(id);

        // Redirect to landing page if session ID does not match
        String requesterSessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        if (queueClient == null) {
            return "redirect:/";
        }

        if (!requesterSessionId.equals(queueClient.getSessionId())) {
            return "redirect:/";
        }

        model.addAttribute("sessionId", queueClient.getSessionId());
        model.addAttribute("progress", queueClient.getProgress());

        return "queue";
    }

    // Join a new Client into the queue
    @RequestMapping(value="/queue/join",method=RequestMethod.POST)
    public String queueJoin(@RequestParam String client, @RequestParam String description, Model model) {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        List<String> tags = Arrays.asList(description.split("\\W+"));

        // Attempt to load existing model (in case rejoining queue)
        QueueClient queueClient = null;
        List<QueueClient> queueClients = queueClientService.findClientsWithSessionId(sessionId);

        if (queueClients.size() > 0) {
            // Only handle first, should only have 1 either way
            queueClient = queueClients.get(0);
        }

        if (queueClient == null) {
            // Need to make a new model
            queueClient = new QueueClient();
            queueClient.setClient(client);
            queueClient.setProgress(0);
            queueClient.setSessionId(sessionId);
            queueClient.setDescription(description);
            queueClient.setStartTime(new Date());
            queueClient.setTags(tags);
            queueClientService.saveClient(queueClient);

            // Notify staff members a new client exists
            messagingTemplate.convertAndSend("/topic/queue/changed", "userMoved");
        }

        return "redirect:/queue/" + queueClient.getId();
    }

    // This page is accessible by any user and returns a page where a client can enter a preferred name and describe
    // what they want to talk about in order to generate a new QueueClient
    @RequestMapping(value="/queue/land",method=RequestMethod.GET)
    public String queueLand() {
        return "queueland";
    }

    // Leave the queue if they are already in it
    @RequestMapping(value="/queue/leave",method=RequestMethod.GET)
    public String queueLeave() {
        String sessionID = RequestContextHolder.currentRequestAttributes().getSessionId();

        // Remove any QueueClients with ID
        queueClientService.removeClientsWithSessionId(sessionID);

        // Notify staff members a client has left
        messagingTemplate.convertAndSend("/topic/queue/changed", "userMoved");
        return "redirect:/queue/land";
    }


    // Scheduled method for updating queue positions periodically and acting as a keep-alive
    @Scheduled(fixedDelay=2000)
    public void updateQueuePositions() {
        List<QueueClient> queueClients = queueClientService.findClientsInOrderAdded();
        // Add 1 to create a queue offset
        Long queueLength = Long.valueOf(queueClients.size()) + 1;
        int place = 1;

        for (QueueClient queueClient : queueClients) {
            double progressDoub = 1000*((queueLength.intValue()-place)*1.0/queueLength.intValue());
            int progress = (int)progressDoub;

            if (queueClient.getProgress() < progress) {
                // Don't want to go backwards, only change if new progress is further
                queueClient.setProgress(progress);
                queueClientService.saveClient(queueClient);

            }

            place++;

            // Set endpoint for websocket unique to this client
            String location = "/topic/queuestatus/"+queueClient.getSessionId();

            // Only send relevant info:
            Map<String, Object> relevantInfo = new HashMap<String, Object>();

            relevantInfo.put("progress", queueClient.getProgress());
            relevantInfo.put("sessionId", queueClient.getSessionId());

            messagingTemplate.convertAndSend(location, relevantInfo);

        }
    }

}
