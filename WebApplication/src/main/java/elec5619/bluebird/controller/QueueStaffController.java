package elec5619.bluebird.controller;

import elec5619.bluebird.domain.*;
import elec5619.bluebird.repository.ProfileRepository;
import elec5619.bluebird.repository.UserRepository;
import elec5619.bluebird.service.QueueClientService;
import elec5619.bluebird.service.QueueStaffService;
import elec5619.bluebird.service.SecurityService;
import elec5619.bluebird.service.UserService;
import elec5619.bluebird.service.impl.UserDetailsServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Josh on 17/10/2016.
 */
@Controller
public class QueueStaffController {

    private final QueueClientService queueClientService;
    private final QueueStaffService queueStaffService;
    private final UserService userService;
    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @SuppressWarnings(value = "unused")
    private final Logger logger = LogManager.getLogger(QueueStaffController.class);

    @Autowired
    public QueueStaffController(QueueClientService queueClientService,
                                QueueStaffService queueStaffService,
                                UserService userService,
                                UserRepository userRepository,
                                ProfileRepository profileRepository,
                                SimpMessagingTemplate simpMessagingTemplate) {
        this.queueClientService = queueClientService;
        this.queueStaffService = queueStaffService;
        this.userService = userService;
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    // Presents the unique staff with a list of matches from the Matching Algorithm and top 3 longest waiting users
    @RequestMapping("/queue/staff/{id}")
    public String queue(@PathVariable Long id, Model model) {
        // Retrieve top 3 clients by longest waiting time
        // Top 3 will be first in this list
        List<QueueClient> queueClients = queueClientService.findClientsInOrderAdded();
        int size = 3;
        if (queueClients.size() < 3) size = queueClients.size();

        // Add relevant objects to model
        for (int i = 0; i < size; i++) {
            // Calculate time
            Date startDate = queueClients.get(i).getStartTime();
            Date nowDate = new Date();
            long diffMs = nowDate.getTime()-startDate.getTime();
            int day = (int)TimeUnit.MILLISECONDS.toDays(diffMs);
            long hours = TimeUnit.MILLISECONDS.toHours(diffMs) - (day *24);
            long minute = TimeUnit.MILLISECONDS.toMinutes(diffMs) - (TimeUnit.MILLISECONDS.toHours(diffMs)* 60);
            String timeStr = minute + " Minutes";
            if (hours > 0) {
                timeStr = hours + " Hours " + minute + " Minutes";
            }

            // Add to model
            model.addAttribute("id_" + i, queueClients.get(i).getId());
            model.addAttribute("name_" + i, queueClients.get(i).getClient());
            model.addAttribute("description_" + i, queueClients.get(i).getDescription());
            model.addAttribute("time_" + i, timeStr);
        }

        // Retrieve top 3 matching clients for a staff member
        QueueStaff queueStaff = queueStaffService.findStaffWithId(id);

        // If it has already been deleted, the client is changing to chat so we can ignore this
        // and place a generic queue staff page
        if (queueStaff == null) {
            return "queuestaff";
        }

        // Iterate through, calculating match parameters

        List<Long> listOfScores = new ArrayList<Long>();
        for (QueueClient queueClient : queueClients) {
            // Calculate score
            List<String> clientTags = queueClient.getTags();
            List<String> staffTags = queueStaff.getTags();

            // Set value of matching tags for staff-client
            Long basicMatchVal = Long.valueOf(10);

            Long totalMatchScore = Long.valueOf(0);
            for (String clientTag : clientTags) {
                Long matchScore = Long.valueOf(0);

                // Calculate between unique tags
                for (String staffTag : staffTags) {
                    if (clientTag.equals(staffTag)) {
                        matchScore = matchScore + basicMatchVal;
                    }
                }

                // Calculate between common tags
                for (QueueTag queueTag : queueStaffService.findAllCommonTags()) {
                    if (clientTag.equals(queueTag.getTag())) {
                        matchScore = matchScore + Long.valueOf(queueTag.getScore());
                    }
                }

                // Add to current score
                totalMatchScore = totalMatchScore + matchScore;

            }

            // Add score
            listOfScores.add(totalMatchScore);
        }

        // Order those scores, adding the three with the highest
        int limit = 3;
        if (listOfScores.size() < 3) limit = listOfScores.size();
        Long lastHighest = Long.valueOf(-1);
        for (int i = 0; i < limit; i++) {
            Long currHighest = Long.valueOf(-1);
            int indexValue = -1;
            for (int j = 0; j < listOfScores.size(); j++) {
                Long tempVal = listOfScores.get(j);
                if (tempVal > currHighest && ((tempVal < lastHighest) || (lastHighest == -1))) {
                    indexValue = j;
                    currHighest = tempVal;
                }

                // Set the lastHighest
                if (j == listOfScores.size() - 1) lastHighest = currHighest;
            }

            // If not suitable matches found, don't add anything (e.g. no matches)
            if (indexValue == -1) continue;

            // Add to model
            model.addAttribute("id_" + i + "_match", queueClients.get(indexValue).getId());
            model.addAttribute("name_" + i + "_match", queueClients.get(indexValue).getClient());
            model.addAttribute("description_" + i + "_match", queueClients.get(indexValue).getDescription());
        }


        model.addAttribute(queueStaff);
        return "queuestaff";
    }

    // Join a staff member to the queue staff-view
    @RequestMapping(value = "/queue/staff/join", method = RequestMethod.GET)
    public String queueJoin(Model model) {
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        // Get current logged in staff member from security principal
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        elec5619.bluebird.domain.User staff = userService.findByUsername(principal.getUsername());
        staff.setSessionId(sessionId);
        userRepository.save(staff);

        Profile profile = profileRepository.findProfileByUser(staff);
        // Stop getting a shared resource error in Hibernate
        ArrayList<String> tags = new ArrayList<>(profile.getTags());

        // Set relevant attributes
        QueueStaff queueStaff = new QueueStaff();
        queueStaff.setStaff(staff.getUsername());
        queueStaff.setTags(tags);
        queueStaffService.saveStaff(queueStaff);

        model.addAttribute(queueStaff);
        return "redirect:/queue/staff/" + queueStaff.getId();
    }

    // Pop a requested user from a queue
    @RequestMapping(value = "/queue/staff/{id}/pop", method = RequestMethod.POST)
    public RedirectView queuePop(@RequestParam String clientid, @PathVariable Long id, RedirectAttributes redirectAttributes) {
        QueueStaff queueStaff = queueStaffService.findStaffWithId(id);

        // Chat session should be made before queueClient is saved (otherwise race condition)
        ChatSession newSession = new ChatSession();
        newSession.setStaffSessionId(RequestContextHolder.getRequestAttributes().getSessionId());
        newSession.setStaffName(queueStaff.getStaff());

        // Signal client that they are finished
        QueueClient queueClient = queueClientService.setClientFinishedWithId(Long.parseLong(clientid));

        // Set User attributes from queueClient
        newSession.setUserSessionId(queueClient.getSessionId());
        newSession.setUserName(queueClient.getClient());
        queueStaffService.saveChatSession(newSession);

        // Remove the QueueStaff model as it is no longer needed
        queueStaffService.deleteStaff(queueStaff);

        // Set session for chat in the GET request
        redirectAttributes.addAttribute("csid", newSession.getId());
        return new RedirectView("/chat/staff");
    }
}
