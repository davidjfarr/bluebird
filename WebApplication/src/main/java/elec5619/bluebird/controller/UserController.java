package elec5619.bluebird.controller;

import elec5619.bluebird.domain.ChatSession;
import elec5619.bluebird.domain.Profile;
import elec5619.bluebird.domain.User;
import elec5619.bluebird.repository.ChatSessionRepository;
import elec5619.bluebird.repository.ProfileRepository;
import elec5619.bluebird.repository.UserRepository;
import elec5619.bluebird.service.SecurityService;
import elec5619.bluebird.service.UserService;
import elec5619.bluebird.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Controller
public class UserController {
    private UserService userService;
    private SecurityService securityService;
    private UserValidator userValidator;
    private ProfileRepository profileRepository;
    private ChatSessionRepository chatRepository;
    private UserRepository userRepository;

    @SuppressWarnings("unused")
    private final static Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService,
                          SecurityService securityService,
                          UserValidator userValidator,
                          ProfileRepository profileRepository,
                          ChatSessionRepository chatRepository,
                          UserRepository userRepository) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
        this.profileRepository = profileRepository;
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    // Method only access
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, Model model) {

        userService.saveStaff(userForm);
        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/logout";
    }

    @RequestMapping(value = "/staff-login", method = RequestMethod.GET)
    public String staffLogin(Model model) {
        return "staff-login";
    }

    @RequestMapping(value = "/staff-login-error", method = RequestMethod.GET)
    public String staffLoginError(Model model) {
        model.addAttribute("loginError", true);
        return "staff-login";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }

    @RequestMapping(value = "/login-processing")
    public String loginProcessing(Model model) {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Collection<GrantedAuthority> authorities = principal.getAuthorities();
        String role = authorities.toString();
        if(principal.getAuthorities().toString().equals("[Staff]")){
            // Check if user has a profile. If not, then they have only been created by admin
            User staff = userService.findByUsername(principal.getUsername());
            if(!profileRepository.userHasProfile(staff)) {
                Profile profile = new Profile("","",new ArrayList<>(Arrays.asList("Divorce", "Anxiety", "Depression")),"");
                profile.setUser(staff);
                profileRepository.save(profile);
                return "redirect:/edit-profile";
            }

            return "redirect:/queue/staff/join";
        }
        else if(principal.getAuthorities().toString().equals("[Admin]")){
            return "redirect:/registration";
        }

        return "redirect:/index";
    }

    @RequestMapping(value = "/edit-profile", method = RequestMethod.GET)
    public String editProfile(Model model) {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User staff = userService.findByUsername(principal.getUsername());
        Profile profile = profileRepository.findProfileByUser(staff);
        model.addAttribute("staffProfile", profile);

        return "edit-profile";
    }

    @RequestMapping(value = "/edit-profile", method = RequestMethod.POST)
    public String editProfile(@ModelAttribute("staffProfile") Profile profile, Model model) {
        profileRepository.save(profile);

        return "redirect:/queue/staff/join";
    }

    @RequestMapping(value = "/view-staff-profile")
    public String viewStaffProfile(Model model) {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        List<ChatSession> chatSessions = chatRepository.findByUserSessionId(sessionId);
        ChatSession session = new ChatSession();
        for (ChatSession i: chatSessions) {
            if (i.getUserSessionId().equals(sessionId)) {
                session = i;
                break;
            }
        }

        System.out.println(session.getUserSessionId());

        User staff = userRepository.findBySessionId(session.getStaffSessionId());
        Profile profile = profileRepository.findProfileByUser(staff);
        model.addAttribute("staff", profile);

        return "view-staff-profile";

    }
}

