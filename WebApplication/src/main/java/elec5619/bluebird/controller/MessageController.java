package elec5619.bluebird.controller;

import elec5619.bluebird.domain.ChatMessage;
import elec5619.bluebird.domain.ChatSession;
import elec5619.bluebird.domain.QueueClient;
import elec5619.bluebird.repository.ChatSessionRepository;
import elec5619.bluebird.repository.QueueClientRepository;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by yoshi on 18/10/2016.
 */

@Controller
public class MessageController {

    private SimpMessagingTemplate template;
    // Repository to obtain QueueClient objects
    private QueueClientRepository queueClientRepository;
    // Repository for chat sessions
    private ChatSessionRepository chatRepository;

    @Inject
    private MessageController(SimpMessagingTemplate template, QueueClientRepository queueClientRepository, ChatSessionRepository chatRepository) {
        this.template = template;
        this.queueClientRepository = queueClientRepository;
        this.chatRepository = chatRepository;
    }

    @RequestMapping(value = "/chat/staff", method = RequestMethod.GET)
    public String joinStaffChat(Model model, @RequestParam long csid) {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        // Find appropriate chat session by staff Session ID
        ChatSession session = null;
        List<ChatSession> chatSessions = chatRepository.findByStaffSessionId(sessionId);
        if (chatSessions.size() > 0) {
            session = chatSessions.get(0);
        } else {
            // If the Session ID isn't found in chat session, return staff to landing page
            return "queuestaffland";
        }

        // Add attributes for chat window.
        model.addAttribute("csid", csid);
        model.addAttribute("sid", sessionId);
        model.addAttribute("username", session.getUserName());
        model.addAttribute("staffname", session.getStaffName());
        model.addAttribute("isstaff", true);
        return "chat";
    }

    @RequestMapping(value = "/chat", method = RequestMethod.GET)
    public String joinClientChat(Model model) {
        // Retrieve Session ID from queue
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        // Retrieve relevant info from queueClient object
        List<QueueClient> queueClients = queueClientRepository.findBySessionId(sessionId);

        QueueClient queueClient = null;
        if (queueClients.size() > 0) {
            // Should only be one, but left for completeness
            queueClient = queueClients.get(0);
        } else {
            return "queueland";
        }

        // Get Chat Session matching user SessionID
        ChatSession session = null;
        List<ChatSession> chatSessions = chatRepository.findByUserSessionId(sessionId);
        if (chatSessions.size() > 0) {
            session = chatSessions.get(0);
        } else {
            // If user does not have a Chat Session, send them back to the queue to acquire one.
            return "queueland";
        }


        // Add attributes for chat window.
        long csid = session.getId();
        model.addAttribute("csid", csid);
        model.addAttribute("sid", sessionId);
        model.addAttribute("username", session.getUserName());
        model.addAttribute("staffname", session.getStaffName());
        model.addAttribute("isstaff", false);


        // Delete queueClient object since they have joined chat
        queueClientRepository.delete(queueClient);
        // Notify staff members
        template.convertAndSend("/topic/queue/changed", "userMoved");
        return "chat";
    }

    // Mapping for socket messages
    @MessageMapping("/chat/{csid}")
    public void chat(@Payload ChatMessage chatMessage, @DestinationVariable String csid) throws Exception {
        template.convertAndSend("/topic/messages/" + csid, chatMessage);
    }
}
