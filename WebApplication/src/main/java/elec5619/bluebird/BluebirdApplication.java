package elec5619.bluebird;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BluebirdApplication {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BluebirdApplication.class, args);
	}
}
