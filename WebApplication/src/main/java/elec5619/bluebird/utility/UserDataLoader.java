package elec5619.bluebird.utility;

import elec5619.bluebird.domain.Profile;
import elec5619.bluebird.domain.QueueTag;
import elec5619.bluebird.domain.Role;
import elec5619.bluebird.domain.User;
import elec5619.bluebird.repository.ProfileRepository;
import elec5619.bluebird.repository.QueueTagRepository;
import elec5619.bluebird.repository.RoleRepository;
import elec5619.bluebird.service.SecurityService;
import elec5619.bluebird.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
class UserDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private RoleRepository roleRepository;
    private ProfileRepository profileRepository;
    private UserService userService;
    private QueueTagRepository queueTagRepository;
    private SecurityService securityService;

    @SuppressWarnings(value = "unused")
    private final static Logger logger = LogManager.getLogger(UserDataLoader.class);

    public UserDataLoader(RoleRepository roleRepository,
                          ProfileRepository profileRepository,
                          UserService userService,
                          QueueTagRepository queueTagRepository,
                          SecurityService securityService) {
        this.roleRepository = roleRepository;
        this.profileRepository = profileRepository;
        this.userService = userService;
        this.queueTagRepository = queueTagRepository;
        this.securityService = securityService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        // Build roles in database
        saveRoles();

        // Create default set of staff and admin users in system
        createUsers();

        // create lists of high priority tags for the queue match-up algorithm
        createTags();
    }

    private void saveRoles() {
        List<Role> roles = new ArrayList<>(Arrays.asList(
                new Role("Client"),
                new Role("Staff"),
                new Role("Admin")
        ));

        roleRepository.save(roles);
    }

    private void createUsers() {
        // Staff Users
        User staff1 = new User();
        staff1.setUsername("davidjfarr");
        staff1.setPassword("password");
        staff1.setPasswordConfirm("password");
        staff1.setEmail("dfar9903@uni.sydney.edu.au");

        Profile staffProfile1 = new Profile("David",
                "Farr",
                new ArrayList<>(Arrays.asList("Divorce", "Anxiety", "Depression")),
                "I am here to help you");

        staffProfile1.setUser(staff1);

        userService.saveStaff(staff1);
        profileRepository.save(staffProfile1);
        securityService.autologin(staff1.getUsername(), staff1.getPasswordConfirm());
        logger.debug("Saved staff ID: " + staff1.getUserId());

        User staff2 = new User();
        staff2.setUsername("adamread");
        staff2.setPassword("password");
        staff2.setPasswordConfirm("password");
        staff2.setEmail("aread@work.com");

        Profile staffProfile2 = new Profile("Admin",
                "Read",
                new ArrayList<>(Arrays.asList("Divorce", "Anxiety", "Depression")),
                "I am here to help you");

        staffProfile2.setUser(staff2);

        userService.saveStaff(staff2);
        profileRepository.save(staffProfile2);
        securityService.autologin(staff2.getUsername(), staff2.getPasswordConfirm());
        logger.debug("Saved staff ID: " + staff2.getUserId());

        User staff3 = new User();
        staff3.setUsername("annemoore");
        staff3.setPassword("password");
        staff3.setPasswordConfirm("password");
        staff3.setEmail("amoore@work.com");

        Profile staffProfile3 = new Profile("Anne",
                "Moore",
                new ArrayList<>(Arrays.asList("Divorce", "Anxiety", "Depression")),
                "I am here to help you");

        staffProfile3.setUser(staff3);

        userService.saveStaff(staff3);
        securityService.autologin(staff3.getUsername(), staff3.getPasswordConfirm());
        profileRepository.save(staffProfile3);
        logger.debug("Saved staff ID: " + staff3.getUserId());

        User staff4 = new User();
        staff4.setUsername("cherylevans");
        staff4.setPassword("password");
        staff4.setPasswordConfirm("password");
        staff4.setEmail("cevans@work.com");

        Profile staffProfile4 = new Profile("Cheryl",
                "Evans",
                new ArrayList<>(Arrays.asList("Divorce", "Anxiety", "Depression")),
                "I am here to help you");

        staffProfile4.setUser(staff4);

        userService.saveStaff(staff4);
        securityService.autologin(staff4.getUsername(), staff4.getPasswordConfirm());
        profileRepository.save(staffProfile4);
        logger.debug("Saved staff ID: " + staff4.getUserId());

        // Admin Users
        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword("password");
        admin.setPasswordConfirm("password");
        admin.setEmail("admin@work.com");

        userService.saveAdmin(admin);
        securityService.autologin(admin.getUsername(), admin.getPasswordConfirm());
        logger.debug("Saved admin ID: " + staff4.getUserId());
    }

    private void createTags() {
        List<QueueTag> queueTags = new ArrayList<>();
        queueTags.add(new QueueTag("suicide", 1000));
        queueTags.add(new QueueTag("death", 100));
        queueTags.add(new QueueTag("kill", 1000));
        queueTags.add(new QueueTag("last", 20));
        queueTags.add(new QueueTag("goodbye", 50));
        queueTags.add(new QueueTag("hurt", 80));
        queueTags.add(new QueueTag("commit", 60));
        queueTags.add(new QueueTag("attempt", 40));
        queueTags.add(new QueueTag("destroy", 15));
        queueTags.add(new QueueTag("lonely", 15));
        queueTags.add(new QueueTag("loneliness", 15));
        queueTags.add(new QueueTag("die", 100));
        queueTags.add(new QueueTag("alone", 100));
        queueTags.add(new QueueTag("fail", 30));
        queueTags.add(new QueueTag("lost", 40));
        queueTags.add(new QueueTag("paranoid", 40));
        queueTags.add(new QueueTag("pessimistic", 30));

        queueTags.forEach(tag -> logger.debug("Added Queue Tag: " + tag.getTag() + ", with rating: " + tag.getScore()));

        queueTagRepository.save(queueTags);
    }
}
