package elec5619.bluebird.domain;

/**
 * Created by David on 21/10/2016.
 */
public class Email {
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String content;
}
