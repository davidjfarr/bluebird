package elec5619.bluebird.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by David on 16/10/2016.
 */
@Entity
@Table(name = "feedback")
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String user;
    private String staff;
    private String content;
    private String date;

    public String getUser() {
        return user;
    }

    public String getStaff() {
        return staff;
    }

    public String getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setStaff(String staff) { this.staff = staff; }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
