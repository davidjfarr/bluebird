package elec5619.bluebird.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 17/10/2016.
 */
@Entity
public class QueueStaff {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String staff;

    // ElementCollection means cascading is done by default
    @ElementCollection(fetch=FetchType.EAGER)
    private List<String> tags;

    public QueueStaff() {

    }

    public QueueStaff(String staff, List<String> tags, Map<QueueClient, Integer> matchScore) {
        this.staff = staff;
        this.tags = tags;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
