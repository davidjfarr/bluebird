package elec5619.bluebird.domain;

/**
 * Created by yoshi on 18/10/2016.
 */
public class ChatMessage {

    private String message;
    private String sender;
    private String sendername;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSendername() {
        return sendername;
    }

    public void setSendername(String sendername) {
        this.sendername = sendername;
    }
}
