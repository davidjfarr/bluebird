package elec5619.bluebird.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "profile")
public class Profile {
    private long id;
    private String firstName;
    private String lastName;

    private User user;

    private List<String> tags = new ArrayList<>();
    private String bio;

    public Profile() {}

    public Profile(String firstName,
                   String lastName,
                   List<String> tags,
                   String bio) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.tags = tags;
        this.bio = bio;
    }

    @Id
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @ElementCollection(targetClass = String.class)
    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
