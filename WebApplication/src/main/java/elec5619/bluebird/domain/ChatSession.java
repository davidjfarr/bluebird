package elec5619.bluebird.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by yoshi on 22/10/2016.
 */

@Entity
public class ChatSession {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String userSessionId;
    private String staffSessionId;
    private String userName;
    private String staffName;

    public ChatSession() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getStaffSessionId() {
        return staffSessionId;
    }

    public void setStaffSessionId(String staffSessionId) {
        this.staffSessionId = staffSessionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
}
