package elec5619.bluebird.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Josh on 12/10/2016.
 */
@Entity
public class QueueClient {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String client;
    private int progress; // Progress out of 1000
    private String sessionId;
    private Date startTime;
    @Column(columnDefinition="text")
    private String description;
    @ElementCollection(fetch=FetchType.EAGER)
    private List<String> tags;

    // Empty constructor
    public QueueClient() {}

    // Alternate constructor
    public QueueClient(Long id, String client, int progress, String sessionId, Date startTime, String description) {
        this.id = id;
        this.client = client;
        this.progress = progress;
        this.sessionId = sessionId;
        this.startTime = startTime;
        this.description = description;
    }

    // Setters
    public void setClient(String client) {
        this.client = client;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    // Getters
    public String getClient() {
        return this.client;
    }

    public Long getId() {
        return this.id;
    }

    public int getProgress() {
        return progress;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getTags() {
        return tags;
    }

}
