package elec5619.bluebird.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Josh on 17/10/2016.
 * This models a tag term as well as an importance score associated with it
 */
@Entity
public class QueueTag {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private int score;
    private String tag;

    public QueueTag() {

    }

    public QueueTag(String tag, int score) {
        this.score = score;
        this.tag = tag;

    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
