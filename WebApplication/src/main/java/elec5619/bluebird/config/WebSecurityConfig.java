package elec5619.bluebird.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public WebSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        // allow static resources
        http.authorizeRequests().antMatchers("/resources/**").permitAll();

        // The queue staff should not be accessible to clients
        http
                .authorizeRequests().antMatchers("/queue/staff/**").authenticated();

        // allow anonymous client controller endpoints
        http
            .authorizeRequests()
            .antMatchers("/",
                    "/finish",
                    "/view-staff-profile",
                    "/welcome",
                    "/messaging/**",
                    "/chat/**",
                    "/queue/**",
                    "/topic/**",
                    "/user/**",
                    "/app/**",
                    "/bootstrap-slider/**",
                    "/img/**",
                    "/finish/**",
                    "/websocket/**").permitAll();



        // secure all other staff and admin site sections
        http
            .authorizeRequests().anyRequest().authenticated()
            .and()
        .formLogin()
        .loginPage("/staff-login")
            .failureUrl("/staff-login-error")
            .usernameParameter("username")
            .passwordParameter("password")
            //.successForwardUrl("/queue/staff/join")
            .successForwardUrl("/login-processing")
        .permitAll()
        .and()
        .logout().permitAll();
        http.exceptionHandling().accessDeniedPage("/403");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
}

