/**
 * Created by David on 18/10/2016.
 */
var map;
var ip = '129.78.56.199';
var pos;
var infowindow;

function initMap() {
    var scriptName = $('script[src*=maps]');
    //ip = String(scriptName.attr('data-address'));

    console.log("Querying infoDB server");
    var key = 'd45d2a3c2fb3299c1296a4d518296c6feb3cebc2beb9d8f19aba155aa18f59d7';
    var url = 'http://api.ipinfodb.com/v3/ip-city/?key=' +  key + '&ip=' + ip + '&format=json';

    getJSON(url, buildMap);
}

function buildMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: pos,
        zoom: 15
    });

    infowindow = new google.maps.InfoWindow();

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infowindow.setPosition(pos);
            infowindow.setContent('Location found.');
            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infowindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        infowindow.setContent('Unable to get location')
        handleLocationError(false, infowindow, map.getCenter());
    }

    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch({
        location: pos,
        radius: 2000,
        keyword: ['mental health']
    }, callback);
}


function callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; results.length > i; i++) {
            createMarker(results[i]);
        }
    }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}

function handleLocationError(browserHasGeolocation, infowindow, pos) {
    infowindow.setPosition(pos);
    infowindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}

function getJSON(url, callback){
    $.getJSON(url, function (d) {
        parsePos(d);
        callback();
    });
}

function parsePos(data) {
    pos = {
        lat: parseFloat(data.latitude), lng: parseFloat(data.longitude)
    };
}
