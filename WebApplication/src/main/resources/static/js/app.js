/**
 * Created by Josh on 14/10/2016.
 */

updateQueue("test 2");

// Connects with a given session id
function connect(sessionid) {
    var stompClient = null;

    // Connect to queue
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('/topic/queuestatus/${pageContext.session.id}: ' + sessionid);
        var uniquePath = '/topic/queuestatus/' + sessionid;
        console.log(socket._transport.url);
        stompClient.subscribe(uniquePath, function (details) {
            updateQueue(JSON.parse(details.body));
        });
    });
}

function connectChange() {
    var stompClient = null;

    // Connect to queue movement updates
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        var uniquePath = '/topic/queue/changed';
        console.log(socket._transport.url);
        stompClient.subscribe(uniquePath, function (details) {
            location.reload();
        });
    });
}

function updateQueue(message) {
    if (message.progress >= 1000) {
        beginChat();
    }
    console.log(message);
    $("#queuestats").html("Have received object: " + message + ".");
    $("#progress").html(message.progress);
    $("#sessionid").html(message.sessionId);
    moveProgress(message.progress);
}

function beginChat() {
    // Sends the user direct to a chat endpoint
    location.replace('/chat');
}

function moveProgress(progress) {
    var maxPoint = progress/1000*100;
    var elem = document.getElementById("queueBar");
    var width = elem.style.width.replace('%', '');
    var id = setInterval(frame, 30);
    function frame() {
        if (width >= maxPoint) {
            clearInterval(id);
        } else {
            width++;
            elem.style.width = width + '%';
        }
    }
}