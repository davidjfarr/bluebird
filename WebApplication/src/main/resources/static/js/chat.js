var stompClient = null;
var csid  = null;
var sid = null;
var username = null;
var staffname = null;
var isstaff = null;

function chat(csid, sid, username, staffname, isstaff) {
    this.csid = csid;
    this.sid = sid;
    this.username = username;
    this.staffname = staffname;
    this.isstaff = isstaff;
    connect();
}

function connect() {
    var socket = new SockJS('/chat');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        var subscription = '/topic/messages/' + csid;
        stompClient.subscribe(subscription, function(message) {
            showMessage(message.body);
        });
    });
}

function disconnect() {
    stompClient.disconnect();
    console.log("Disconnected");
}

function showMessage(message) {
    var data = JSON.parse(message);
    if (data.sender == sid) {
        if (data.sendername == staffname) {
            $("#messages").append("<tr style=\"background-color:PowderBlue\"><td>" + "<b>" + staffname + ": " + "</b>" + data.message + "</tr></td>");
        } else {
            $("#messages").append("<tr style=\"background-color:PowderBlue\"><td>" + "<b>" + username + ": " + "</b>" + data.message + "</tr></td>");
        }
    } else {
        if (data.sendername == staffname) {
            $("#messages").append("<tr><td>" + "<b>" + staffname + ": " + "</b>" + data.message + "</tr></td>");
        } else {
            $("#messages").append("<tr><td>" + "<b>" + username + ": " + "</b>" + data.message + "</tr></td>");
        }

    }

}

function sendMessage() {
    var sendername = username;
    if (isstaff) {
        sendername = staffname;
    }
    var path = "/app/chat/" + csid;
    stompClient.send(path, {}, JSON.stringify({
        'message': $("#message").val(),
        'sender': sid,
        'sendername': sendername
    }));
}

function clearField() {
    document.getElementById("message").value="";
}

$(function () {
    $( "#send" ).click(function() { sendMessage(); clearField(); });
    $("#message").keydown(function(event) {
        if (event.keyCode == 13) {
            sendMessage();
                clearField();
        }
    });
});