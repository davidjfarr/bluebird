package elec5619.bluebird.Chat.domain;

import elec5619.bluebird.domain.ChatMessage;
import junit.framework.TestCase;

/**
 * Created by yoshi on 25/10/2016.
 */
public class ChatMessageTest extends TestCase {
    private ChatMessage message;

    protected void setUp() {
        message = new ChatMessage();
    }

    public void testSetGetMessage() {
        String messageString = "foo bar";
        message.setMessage(messageString);
        assertEquals(messageString, message.getMessage());
    }

    public void testSetGetSender() {
        String sender = "ABC";
        message.setSender(sender);
        assertEquals(sender, message.getSender());
    }

    public void testSetGetSenderName() {
        String senderName = "SenderName";
        message.setSendername(senderName);
        assertEquals(senderName, message.getSendername());
    }
}
