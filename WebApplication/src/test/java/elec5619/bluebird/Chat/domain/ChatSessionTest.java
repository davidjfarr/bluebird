package elec5619.bluebird.Chat.domain;

import elec5619.bluebird.domain.ChatSession;
import junit.framework.TestCase;

/**
 * Created by yoshi on 25/10/2016.
 */
public class ChatSessionTest extends TestCase {
    private ChatSession session;

    protected void setUp() throws Exception {
        session = new ChatSession();
    }

    public void testAutoSessionId() {
        assertEquals(Long.getLong("1"), session.getId());
    }

    public void testGetSetSession() {
        String staffID = "AAAA";
        String userID = "BBBB";

        session.setStaffSessionId(staffID);
        session.setUserSessionId(userID);

        assertEquals(staffID, session.getStaffSessionId());
        assertEquals(userID, session.getUserSessionId());
    }

    public void testGetSetUser() {
        String user = "foo";
        String staff = "bar";

        session.setUserName(user);
        session.setStaffName(staff);

        assertEquals(user, session.getUserName());
        assertEquals(staff, session.getStaffName());
    }
}
