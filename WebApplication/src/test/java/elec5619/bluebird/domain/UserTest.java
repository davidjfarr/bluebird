package elec5619.bluebird.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by David on 25/10/2016.
 */
@SpringBootTest
public class UserTest {

    @Autowired
    private User user;

    @Before
    public void setup() {
        user = new User();
    }

    @Test
    public void testGetUsername() {
        user.setUsername("davidjfarr");
        assertEquals(user.getUsername(), "davidjfarr");
    }

    @Test
    public void testGetSessionId(){
        user.setSessionId("SessionId");
        assertEquals(user.getSessionId(), "SessionId");
    }

    @Test
    public void testGetPassword() {
        user.setPassword("password");
        assertEquals(user.getPassword(), "password");
    }

    @Test
    public void testGetPasswordConfirm() {
        user.setPasswordConfirm("password");
        assertEquals(user.getPasswordConfirm(), "password");
    }
}