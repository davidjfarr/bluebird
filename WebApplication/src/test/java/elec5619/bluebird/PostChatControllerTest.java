package elec5619.bluebird;

import elec5619.bluebird.domain.Email;
import elec5619.bluebird.domain.Feedback;
import elec5619.bluebird.repository.FeedbackRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertEquals;

/**
 * Created by David on 25/10/2016.
 */
public class PostChatControllerTest {
    @Autowired

    @MockBean
    BluebirdApplication mockApplication;

    @Test
    public void testFeedbackCreation() {
        Feedback feedback = new Feedback();
        feedback.setStaff("Davo");
        feedback.setUser("Bruce");
        feedback.setContent("Yeah alright");

        assertEquals("Bruce", feedback.getUser());
        assertEquals("Davo", feedback.getStaff());
        assertEquals("Yeah alright", feedback.getContent());
    }

    @Test
    public void testEmailCreation() {
        Email email = new Email();
        email.setContent("Test email");

        assertEquals("Test email", email.getContent());
    }
}
