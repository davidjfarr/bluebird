package elec5619.bluebird.Queue.domain;

import elec5619.bluebird.domain.QueueStaff;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Josh on 24/10/16.
 */
public class QueueStaffTest extends TestCase {

    private QueueStaff queueStaff;

    protected void setUp() throws Exception {
        queueStaff = new QueueStaff();
    }

    public void testSetAndGetStaff() {
        String testClient = "Test Staff";
        assertNull(queueStaff.getStaff());
        queueStaff.setStaff(testClient);
        assertEquals(testClient, queueStaff.getStaff());
    }

    public void testSetAndGetId() {
        Long testId = Long.valueOf(50);
        assertNull(queueStaff.getId());
        queueStaff.setId(testId);
        assertEquals(testId, queueStaff.getId());
    }

    public void testSetAndGetTags() {
        List <String> testTags = Arrays.asList("Test", "Description", "of", "many", "words");
        assertNull(queueStaff.getTags());
        queueStaff.setTags(testTags);
        assertEquals(testTags, queueStaff.getTags());
    }

}
