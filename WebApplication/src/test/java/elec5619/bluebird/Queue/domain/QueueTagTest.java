package elec5619.bluebird.Queue.domain;

import elec5619.bluebird.domain.QueueTag;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Josh on 24/10/16.
 */
public class QueueTagTest extends TestCase {

    private QueueTag queueTag;

    protected void setUp() throws Exception {
        queueTag = new QueueTag();
    }

    public void testSetAndGetTag() {
        String testTag = "TestTag";
        assertNull(queueTag.getTag());
        queueTag.setTag(testTag);
        assertEquals(testTag, queueTag.getTag());
    }

    public void testSetAndGetId() {
        Long testId = Long.valueOf(50);
        assertNull(queueTag.getId());
        queueTag.setId(testId);
        assertEquals(testId, queueTag.getId());
    }

    public void testSetAndGetScore() {
        int score = 49;
        assertEquals(0, queueTag.getScore());
        queueTag.setScore(score);
        assertEquals(score, queueTag.getScore());
    }

}
