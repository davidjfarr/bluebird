package elec5619.bluebird.Queue.domain;

import elec5619.bluebird.domain.QueueClient;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Josh on 24/10/16.
 */
public class QueueClientTest extends TestCase {

    private QueueClient queueClient;

    protected void setUp() throws Exception {
        queueClient = new QueueClient();
    }

    public void testSetAndGetClient() {
        String testClient = "Test Client";
        assertNull(queueClient.getClient());
        queueClient.setClient(testClient);
        assertEquals(testClient,queueClient.getClient());
    }

    public void testSetAndGetId() {
        Long testId = Long.valueOf(50);
        assertNull(queueClient.getId());
        queueClient.setId(testId);
        assertEquals(testId, queueClient.getId());
    }
    public void testSetAndGetProgress() {
        int progress = 100;
        assertEquals(0,queueClient.getProgress());
        queueClient.setProgress(progress);
        assertEquals(progress,queueClient.getProgress());
    }
    public void testSetAndGetSessionId() {
        String sessionId = "TEST12345678901234567890";
        assertNull(queueClient.getSessionId());
        queueClient.setSessionId(sessionId);
        assertEquals(sessionId,queueClient.getSessionId());
    }
    public void testSetAndGetStartTime() {
        Date testDate = new Date();
        assertNull(queueClient.getStartTime());
        queueClient.setStartTime(testDate);
        assertEquals(testDate,queueClient.getStartTime());
    }
    public void testSetAndGetDescription() {
        String testDescription = "Test Description of many words";
        assertNull(queueClient.getDescription());
        queueClient.setDescription(testDescription);
        assertEquals(testDescription,queueClient.getDescription());
    }
    public void testSetAndGetTags() {
        List<String> testTags = Arrays.asList("Test", "Description", "of", "many", "words");
        assertNull(queueClient.getTags());
        queueClient.setTags(testTags);
        assertEquals(testTags,queueClient.getTags());
    }

}
