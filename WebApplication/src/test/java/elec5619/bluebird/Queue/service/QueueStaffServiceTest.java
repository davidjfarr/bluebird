package elec5619.bluebird.Queue.service;

import elec5619.bluebird.domain.QueueStaff;
import elec5619.bluebird.domain.QueueTag;
import elec5619.bluebird.repository.ChatSessionRepository;
import elec5619.bluebird.repository.QueueStaffRepository;
import elec5619.bluebird.repository.QueueTagRepository;
import elec5619.bluebird.service.QueueStaffService;
import elec5619.bluebird.service.impl.QueueStaffServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Josh on 25/10/16.
 */
public class QueueStaffServiceTest {

    @Autowired
    private QueueStaffService queueStaffService;
    private QueueStaffRepository queueStaffRepositoryMock;
    private ChatSessionRepository chatSessionRepositoryMock;
    private QueueTagRepository queueTagRepositoryMock;

    Long id = Long.valueOf(10);
    QueueStaff queueStaff = new QueueStaff();
    Iterable<QueueTag> queueTags;

    @Before
    public void setUp() {
        queueStaffRepositoryMock = Mockito.mock(QueueStaffRepository.class);
        chatSessionRepositoryMock = Mockito.mock(ChatSessionRepository.class);
        queueTagRepositoryMock = Mockito.mock(QueueTagRepository.class);
        queueStaffService = new QueueStaffServiceImpl(queueStaffRepositoryMock, chatSessionRepositoryMock, queueTagRepositoryMock);

        queueStaff.setStaff("Staff");
        queueStaff.setTags(Arrays.asList("a", "b"));
        queueStaff.setId(id);

        Mockito.when(queueStaffService.findStaffWithId(id)).thenReturn(queueStaff);
        queueTags = Arrays.asList(new QueueTag("a", 1), new QueueTag("b", 2));
        Mockito.when(queueStaffService.findAllCommonTags()).thenReturn(queueTags);
    }

    @Test
    public void testSaveAndLoadQueueClient() throws Exception {
        queueStaffService.saveStaff(queueStaff);

        QueueStaff loadedQueueStaff = queueStaffService.findStaffWithId(id);
        assertEquals(id,loadedQueueStaff.getId());
    }

    @Test
    public void testLoadsTags() throws Exception {
        Iterable<QueueTag> loadedTags = queueStaffService.findAllCommonTags();
        assertEquals(queueTags,loadedTags);
    }

}
