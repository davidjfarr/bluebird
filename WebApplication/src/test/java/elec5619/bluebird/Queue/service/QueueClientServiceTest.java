package elec5619.bluebird.Queue.service;

import elec5619.bluebird.domain.QueueClient;
import elec5619.bluebird.repository.QueueClientRepository;
import elec5619.bluebird.service.QueueClientService;
import elec5619.bluebird.service.impl.QueueClientServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Josh on 25/10/16.
 */
public class QueueClientServiceTest {

    @Autowired
    private QueueClientService queueClientService;
    private QueueClientRepository queueClientRepositoryMock;

    Long id = Long.valueOf(10);
    String sessionId = "TESTSESSION";
    QueueClient queueClient = new QueueClient();
    QueueClient savedClient = queueClient;

    @Before
    public void setUp() {
        queueClientRepositoryMock = Mockito.mock(QueueClientRepository.class);
        queueClientService = new QueueClientServiceImpl(queueClientRepositoryMock);

        queueClient.setStartTime(new Date());
        queueClient.setClient("Client");
        queueClient.setDescription("a b");
        queueClient.setTags(Arrays.asList("a", "b"));
        queueClient.setSessionId(sessionId);
        queueClient.setProgress(0);
        queueClient.setId(id);

        QueueClient finishedMock = new QueueClient();
        finishedMock.setProgress(1000);

        Mockito.when(queueClientService.findClientWithId(id)).thenReturn(savedClient);
        Mockito.when(queueClientService.findClientsWithSessionId(sessionId)).thenReturn(Arrays.asList(savedClient));
        Mockito.when(queueClientService.setClientFinishedWithId(id)).thenReturn(finishedMock);
    }

    @Test
    public void testSaveAndLoadQueueClient() throws Exception {
        queueClientService.saveClient(queueClient);

        QueueClient loadedQueueClient = queueClientService.findClientWithId(id);
        assertEquals(id,loadedQueueClient.getId());
    }

    @Test
    public void testFindSessionId() throws Exception {
        queueClientService.saveClient(queueClient);

        List<QueueClient> loadedQueueClients = queueClientService.findClientsWithSessionId(sessionId);
        assertEquals(id,loadedQueueClients.get(0).getId());
    }

    @Test
    public void testSetClientFinished() throws Exception {
        queueClientService.saveClient(queueClient);

        QueueClient loadedQueueClient = queueClientService.findClientWithId(id);
        assertEquals(1000,loadedQueueClient.getProgress());
    }

}
