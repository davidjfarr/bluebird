################################
#    QUEUE INFORMATION V1.0    #
################################

Relevant access points & their descriptions: [A] indicates accessible
1) /queue/land [A]
 > This page is the landing point for clients. Note: For different sessions you need to open different sessions (use incognito)
2) /queue
 > This page displays a client's progress in the queue whilst they wait
3) /queue/staff/land [A]
 > This page emulates landing point to make a staff member
4) /queue/staff
 > This page is where staff select a client to begin a chat session with